package com.maximus.service.impl;

import com.maximus.Application;
import com.maximus.domain.User;
import com.maximus.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @Auther:
 * @Date:
 * @Description:
 */
@SpringBootTest(classes = {Application.class})
@RunWith(SpringRunner.class)
public class UserServiceImplTest {
    @Autowired
    private UserService userService;

    @Test
    public void findByName() {
        User user = userService.findByName("eric");
        System.out.println(user);
    }
}