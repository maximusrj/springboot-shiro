package com.maximus.service.impl;

import com.maximus.domain.User;
import com.maximus.mapper.UserMapper;
import com.maximus.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Auther:
 * @Date:
 * @Description:
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

  private final UserMapper userMapper;
  @Autowired
  public UserServiceImpl(UserMapper userMapper) {
    this.userMapper = userMapper;
  }


  @Override
    public User findByName(String name) {
        return userMapper.findByName(name);
    }

  @Override
  public User findById(Integer id) {
    return userMapper.findById(id);
  }
}
